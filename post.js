const getEverything = () =>{
  return fetch('http://localhost:3000/db')
  .then(response => response.json())
}
const getPosts = () =>{
  return fetch('http://localhost:3000/posts')
  .then(response => response.json())
}
const getUsers = () =>{
  return fetch('http://localhost:3000/users')
  .then(response => response.json())
}

getTitle()
async function getTitle(){
  const posts = await getPosts()
  const users = await getUsers()
  const postKey = location.search.slice(5)
  document.title = 'Post details';
  const post = posts.find(post=>post['id']===Number(postKey))
  const user = users.find(author=>author['id']===post.authorId)

  document.getElementById('page-banner').childNodes[3].textContent = "Title : " + post.title.charAt(0).toUpperCase() + post.title.slice(1)

  document.getElementById('post-list').childNodes[0].textContent = "Email : " + user.email.charAt(0).toUpperCase() + user.email.slice(1)
  document.getElementById('post-list').childNodes[1].textContent = "Author : " + user.name.charAt(0).toUpperCase() + user.name.slice(1)

  document.getElementById('post-list').childNodes[3].childNodes[1].textContent = post.body.charAt(0).toUpperCase() + post.body.slice(1)
  
}