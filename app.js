const getEverything = () =>{
  return fetch('http://localhost:3000/db')
  .then(response => response.json())
}
const getPosts = () =>{
  return fetch('http://localhost:3000/posts')
  .then(response => response.json())
}
const getUsers = () =>{
  return fetch('http://localhost:3000/users')
  .then(response => response.json())
}

function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
}
// Initialize list of dataset
getEverything().then((everything)=>{
var mainList = document.getElementById("navp");
var data = shuffle(everything.posts);
  for(var i=0;i<everything.posts.length;i++){
    var item = data[i];
    var elem = document.createElement("li");
    var childElem = document.createElement("span");
    var childElem1 = document.createElement("span");
    var childElem2 = document.createElement("p");
    var childBody = document.createElement("span")
    var childHiddenId = document.createElement("span")
        
    childHiddenId.style.visibility = "hidden"
    childHiddenId.innerHTML = item['id']

    childBody.style.visibility = "hidden"
    childBody.innerHTML = item['body']
    

    childElem1.className = "delete";
    childElem1.innerHTML = "Delete";

    childElem1.onclick = function(e){
      e.preventDefault();
      
      var parent = this.parentNode;
      fetch(`http://localhost:3000/posts/${this.parentNode.getElementsByTagName("span")[3].innerHTML}`, {
        method: 'delete'
      }).then(response =>
        response.json().then(json => {
          return json;
        }))
      parent.parentNode.removeChild(parent)
    }

    childElem2.className = "name";
    childElem2.innerHTML = everything.users.find(author=> author['id']===item['authorId']).name;
    
    
    childElem.className = "name";
    childElem.innerHTML = item['title'].charAt(0).toUpperCase() + item['title'].slice(1);

    elem.appendChild(childElem2);
    elem.appendChild(childElem);
    elem.appendChild(childElem1);
    elem.appendChild(childBody)
    elem.appendChild(childHiddenId);



    var a = document.createElement("a");
    a.href="postDetails.html"
    a.style="text-decoration: none;color:inherit;"
    a.onclick= function(e){
      let f = this.parentNode.getElementsByTagName("span")[3].innerHTML;
      location.href=this.href+'?key='+ f;
      return false     
    }

    a.appendChild(elem)
    

    mainList.appendChild(a);
  }
    
})


function searchFunction() {
  
    var input = document.getElementById("search-post");  
    var filter = input.value.toUpperCase()

    var ul = document.getElementById("navp");
    var li = ul.getElementsByTagName("li");

    

    for (i = 0; i < li.length; i++) {
      key1 = li[i].getElementsByTagName("p")[0];
      key2 = li[i].getElementsByTagName("span")[0];
      key3 = li[i].getElementsByTagName("span")[2]; 
      if (key1.innerHTML.toUpperCase().indexOf(filter) > -1 || key2.innerHTML.toUpperCase().indexOf(filter) > -1||key3.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
    }
}

document.getElementById("add-post").addEventListener("submit", function(event){
  event.preventDefault()

});

async function addPost(){
  var title = window.document.getElementById("add").value.charAt(0).toUpperCase() + window.document.getElementById("add").value.slice(1).toLowerCase()
  if(title!=""){
  const response = await fetch('https://jsonplaceholder.typicode.com/posts');
  const data = await response.json();
  const posts = await getPosts();
  const users = await getUsers();
  const a = Math.floor(Math.random() * (100 - 1 + 1)) + 1;
  const body = data.find(item=>item['id']===a).body
  


  const settings = {
    method: 'POST',
    headers: {  
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "authorId": Math.floor(Math.random() * (4 - 1 + 1)) + 1,
      "id": posts.slice(-1)[0].id+1,
      "title": title,
      "body": body
    
    })
  };
  
  const res = await fetch(`http://localhost:3000/posts`,settings)
  .then(response => response.json())
  .then(json => {
      return json;
  })

  var item = {
    "authorId": Math.floor(Math.random() * (4 - 1 + 1)) + 1,
    "id": posts.slice(-1)[0].id+1,
    "title": title,
    "body": body
  }
  var mainList = document.getElementById("navp");
  
  var elem = document.createElement("li");
  var childElem = document.createElement("span");
  var childElem1 = document.createElement("span");
  var childElem2 = document.createElement("p");
  var childBody = document.createElement("span")
  var childHiddenId = document.createElement("span")
  
  childHiddenId.style.visibility = "hidden"
  childHiddenId.innerHTML = item['id']
  
  childBody.style.visibility = "hidden"
  childBody.innerHTML = item['body']
  

  childElem1.className = "delete";
  childElem1.innerHTML = "Delete";

  childElem1.onclick = function(e){
    e.preventDefault();
    var parent = this.parentNode;
    fetch(`http://localhost:3000/posts/${this.parentNode.getElementsByTagName("span")[3].innerHTML}`, {
      method: 'delete'
    }).then(response =>
      response.json().then(json => {
        return json;
      }))
    parent.parentNode.removeChild(parent)
  }

  childElem2.className = "name";
  childElem2.innerHTML = users.find(author=> author['id']===item['authorId']).name;
  
  
  childElem.className = "name";
  childElem.innerHTML = item['title'].charAt(0).toUpperCase() + item['title'].slice(1);

  elem.appendChild(childElem2);
  elem.appendChild(childElem);
  elem.appendChild(childElem1);
  elem.appendChild(childBody);
  elem.appendChild(childHiddenId);


  var hrefElem = document.createElement("a");
  hrefElem.href="postDetails.html"
  hrefElem.style="text-decoration: none;color:inherit;"
  

  hrefElem.appendChild(elem)
  

  mainList.insertAdjacentElement('afterbegin',hrefElem);
  
  window.document.getElementById("add").blur();
  window.document.getElementById("add").value = '';
  return res
}

}
