# Zensoft Test Assignment

> Supported functionalities:
***
Paginated list of Post’s (Title and Author)
***
Search by author, title, body
***
Individual Post page with post, author details
***
Delete post
***
Edit post

## Installation

In package.json you could see

```sh
  "scripts": {
    "json:server": "json-server --watch db.json",
    "start": "http-server"
  }
```

So to start JSON Server

```sh
	npm run json:server
```
Also when doing requests, it's good to know that:
If user makes POST, PUT, PATCH or DELETE requests, changes will be automatically and safely saved to db.json in the root of the project.
***
http-server is a simple, zero-configuration command-line http server. It is powerful enough for production usage.

Installation and start via npm:

```sh
	npm install http-server -g
	npm start
```



## Usage example

In modern Front-End development most use-cases are: fetch data from remote API, filter / sort / aggregate data, build abstraction layers between UI and API handlers. In following test assignment you have opportunity to show this skills.

## Providing
The app is be able to filter posts “in-memory”.
***
The tool doesn't use any additional dependencies. Using only built-in libraries of the browser.

